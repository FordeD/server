"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WEAPONS = [[],
    ["weapon_pistol",
        "weapon_CombatPistol",
        "weapon_Pistol50",
        "weapon_SNSPistol",
        "weapon_HeavyPistol",
        "weapon_VintagePistol",
        "weapon_MarksmanPistol",
        "weapon_revolver",
        "weapon_APPistol",
        "weapon_StunGun",
        "weapon_FlareGun"],
    ["weapon_MachinePistol",
        "weapon_SMG",
        "weapon_AssaultSMG",
        "weapon_CombatPDW",
        "weapon_MG",
        "weapon_CombatMG",
        "weapon_Gusenberg",
        "weapon_MiniSMG"],
    ["weapon_assaultrifle",
        "weapon_carbinerifle",
        "weapon_advancedrifle",
        "weapon_specialcarbine",
        "weapon_bullpuprifle",
        "weapon_compactrifle"],
    ["weapon_SniperRifle",
        "weapon_heavySniper",
        "weapon_MarksmanRifle"],
    ["weapon_pumpShotgun",
        "weapon_SawnoffShotgun",
        "weapon_bullpupshotgun",
        "weapon_assaultshotgun",
        "weapon_musket",
        "weapon_HeavyShotgun",
        "weapon_AutoShotgun"],
    ["weapon_grenadelauncher",
        "weapon_RPG",
        "weapon_Minigun",
        "weapon_Firework",
        "weapon_Railgun",
        "weapon_HomingLauncher"],
    ["weapon_Grenade",
        "weapon_StickyBomb",
        "weapon_Proximitymine",
        "weapon_BZGas",
        "weapon_Molotov",
        "weapon_Flare",
        "weapon_PetrolCan",
        "weapon_FireExtinguisher"]
];
//export = WEAPONS; 
