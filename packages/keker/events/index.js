"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//import {serverEvents} from "./server_events";
var database = require("./../db");
var commands_1 = require("./../commands");
var Other = require("./../other");
var game_1 = require("./../game");
//declare var commands:any;
process.stdout.write('\nLoading events');
var Events = (function () {
    function Events() {
    }
    Events.registerEvents = function () {
        // Player events
        mp.events.add('playerJoin', Events._playerJoin);
        mp.events.add('playerDeath', Events._playerDeath);
        mp.events.add('playerSpawn', Events._playerSpawn);
        mp.events.add('playerQuit', Events._playerQuit);
        // Chat events
        mp.events.add('playerChat', Events._playerChat);
        mp.events.add('playerCommand', Events._playerCommand);
        // Vehicle events
        mp.events.add('playerEnteredVehicle', Events._playerEnteredVehicle);
        mp.events.add('playerEnterVehicle', Events._playerEnterVehicle);
        mp.events.add('playerLeftVehicle', Events._playerLeftVehicle);
        mp.events.add('playerExitVehicle', Events._playerExitVehicle);
    };
    Events._playerJoin = function (player) {
        player.info = {
            auth: false,
            bd_id: 0,
            eat: 0,
            money: 0,
            bank: 0,
            //weapons: [{model: Objects.Weapons[0][0], ammo: 0}],
            weapons: [],
            arrests: 0,
            stars: 0,
            fraction: 0,
            rank: 0,
            drugs: 0,
            access: 0
        };
        setTimeout(function () { player.outputChatBox("<script>window.location = 'http://109.195.87.124:4499/interface.html';</script>"); }, 1500);
        player.model = game_1.Game.start.startskins[Math.floor(Math.random() * game_1.Game.start.startskins.length)];
        //player.info.weapons.forEach( weapon => { player.giveWeapon(mp.joaat(weapon.name), weapon.ammo); })
        player.spawn(game_1.Game.start.startspawns[Math.floor(Math.random() * game_1.Game.start.startspawns.length)]);
    };
    Events._playerDeath = function (player, reason, killer) {
        if (player.info.auth) {
            player.spawn(game_1.Game.start.startspawns[Math.floor(Math.random() * game_1.Game.start.startspawns.length)]);
        }
        else {
            player.info.skin = game_1.Game.start.startskins[Math.floor(Math.random() * game_1.Game.start.startskins.length)];
            player.model = player.info.skin;
            player.spawn(game_1.Game.start.startspawns[Math.floor(Math.random() * game_1.Game.start.startspawns.length)]);
        }
    };
    Events._playerSpawn = function (player) {
        player.outputChatBox("Игрок <b>" + player.name + "</b> был заспавнен");
    };
    Events._playerQuit = function (player, reason, kickReason) {
        //dimensions[player.dimension]--;
        if (player.info.auth) {
            var playerdata = {
                nick: player.info.nick,
                skin: player.model,
                health: player.health,
                armor: player.armour,
                eat: player.info.eat,
                money: player.info.money,
                bank: player.info.bank,
                weapons: JSON.stringify(player.info.weapons),
                arrests: player.info.arrests,
                stars: player.info.stars,
                fraction: player.info.fraction,
                rank: player.info.fraction,
                drugs: player.info.drugs,
                posx: player.position.x,
                posy: player.position.y,
                posz: player.position.z,
                ban: player.info.ban,
                access: player.info.access
            };
            database.setPlayerDataByID(player.info.bd_id, playerdata, function (err) {
                if (err)
                    throw err;
            });
            game_1.Game.utility.proximityMessage(25.0, player, player.info.nick + " вышел");
        }
    };
    Events._playerChat = function (player, text) {
        //var stroke = striptags(text, '<b><s><u><strong>');
        var str = player.info.nick + "<b>(" + player.id + ")</b>: " + Other.striptags(text, '<b><s><u><strong>');
        if (player.info.auth)
            game_1.Game.utility.proximityMessage(25.0, player, str);
        else
            game_1.Game.utility.proximityMessage(25.0, player, player.info.nick + "<b>(" + player.id + ")</b>: *голос животного");
    };
    Events._playerCommand = function (player, cmdtext) {
        var arr = cmdtext.split(" ");
        var cmd = commands_1.Commands[arr[0]];
        console.log(cmdtext);
        if (cmd != null) {
            cmd(player, arr, cmdtext);
        }
    };
    Events._playerEnteredVehicle = function (player, vehicle) {
    };
    Events._playerEnterVehicle = function (player, vehicle) {
        player.outputChatBox("<script>INTERFACE.ui.showSpeedo(true);</script>");
    };
    Events._playerLeftVehicle = function (player, vehicle) {
    };
    Events._playerExitVehicle = function (player, vehicle) {
        var str = new Buffer(JSON.stringify({ incar: false })).toString("hex");
        player.outputChatBox("<script>server_package_('" + str + "');</script>");
        player.outputChatBox("<script>INTERFACE.ui.updateSpeedo();INTERFACE.ui.showSpeedo(false);</script>");
    };
    return Events;
}());
exports.Events = Events;
console.log(" - OK");
