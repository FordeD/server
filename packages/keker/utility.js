"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Utility;
(function (Utility) {
    var Sphere = (function () {
        function Sphere(x, y, z, radius) {
            this.x = x;
            this.y = y;
            this.z = z;
            this.radius = radius;
        }
        Sphere.prototype.inRangeOfPoint = function (position) {
            return (Math.pow((position.x - this.x), 2) +
                Math.pow((position.y - this.y), 2) +
                Math.pow((position.z - this.z), 2) < Math.pow(this.radius, 2));
        };
        return Sphere;
    }());
    Utility.Sphere = Sphere;
    function PlayerToPoint(range, player, x, y, z) {
        return new Sphere(x, y, z, range).inRangeOfPoint(player.position);
    }
    Utility.PlayerToPoint = PlayerToPoint;
    function proximityMessage(radi, sender, message) {
        mp.players.forEach(function (_player) {
            if (Utility.PlayerToPoint(radi, _player, sender.position.x, sender.position.y, sender.position.z) /* && receiver.dimension == sender.dimension*/) {
                _player.outputChatBox(message);
            }
        });
    }
    Utility.proximityMessage = proximityMessage;
})(Utility = exports.Utility || (exports.Utility = {}));
