"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var commands_1 = require("./commands");
console.log('\nLoading commands');
exports.Commands = {};
console.log('\t* [ADMIN]');
for (var key in commands_1.serverCommands.admin) {
    process.stdout.write('\t\t\"' + key + '\"');
    exports.Commands[key] = commands_1.serverCommands.admin[key];
    console.log(" - OK");
}
;
console.log('\t* [SUPPORT]');
for (var key in commands_1.serverCommands.support) {
    process.stdout.write('\t\t\"' + key + '\"');
    exports.Commands[key] = commands_1.serverCommands.support[key];
    console.log(" - OK");
}
;
console.log('\t* [PLAYER]');
for (var key in commands_1.serverCommands.player) {
    process.stdout.write('\t\t\"' + key + '\"');
    exports.Commands[key] = commands_1.serverCommands.player[key];
    console.log(" - OK");
}
;
