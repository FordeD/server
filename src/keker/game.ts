import {Objects} from "./objects";
import {Utility} from "./utility";

export namespace Game {
	export var start:Start = {
		startspawns: [new mp.Vector3(-425.517, 1123.620, 325.8544), new mp.Vector3(-415.777, 1168.791, 325.854)],
	    //startskins: [mp.joaat("a_c_chickenhawk"), mp.joaat("a_c_chop"), mp.joaat("a_c_coyote"), mp.joaat("a_c_husky")]
        startskins: [Objects.Models[1], Objects.Models[3], Objects.Models[6], Objects.Models[11]]
	};
    export var utility = Utility;
}