
import {serverCommands} from "./commands";

console.log('\nLoading commands'); 
export var Commands:any = {};

console.log('\t* [ADMIN]'); 
for (var key in serverCommands.admin) {
    process.stdout.write('\t\t\"' + key + '\"');
	Commands[key] = serverCommands.admin[key];
	console.log(" - OK");  
};

console.log('\t* [SUPPORT]'); 
for (var key in serverCommands.support) {
    process.stdout.write('\t\t\"' + key + '\"');
	Commands[key] = serverCommands.support[key];
	console.log(" - OK");  
};

console.log('\t* [PLAYER]'); 
for (var key in serverCommands.player) {
    process.stdout.write('\t\t\"' + key + '\"');
	Commands[key] = serverCommands.player[key];
	console.log(" - OK");  
};
