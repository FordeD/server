import * as Other from "./../other";
import {Game} from "./../game";
import * as database from "./../db";
import {Objects} from "./../objects";

export var serverCommands:Commands = {
	admin: {
		"gopos": (player:PlayerMp, args:any[]) => {
			if (player.info.auth) {
				player.position = new mp.Vector3(parseFloat(args[1]),parseFloat(args[2]),parseFloat(args[3])); 
				player.outputChatBox("X: <b>" + player.position.x + "</b>, Y: <b>" + player.position.y + "</b>, Z: <b>" + player.position.z + "</b>");
			} else Game.utility.proximityMessage(25.0, player, player.info.nick + "<b>(" + player.id + ")</b>: *Голос животного");
		},

		"all": (player:PlayerMp, args:any[]) => {
			if (player) {
				if (player.info.auth) {
					if (player.info.access >= 4) {
						args.shift();
						var mess:string = args.join(' ');
						var str:string = "<font style='color:red;'>[Админ] "+player.name +" говорит: "+mess+"</font>";
						mp.players.forEach(_player => { _player.outputChatBox(str); });
					} else player.outputChatBox("[Ошибка] не достаточно прав.");
				} else Game.utility.proximityMessage(25.0, player, player.info.nick + "<b>(" + player.id + ")</b>: *Голос животного");
			} else {
				args.shift();
				var mess:string = args.join(' ');
				var str:string = "<font style='color:red;'>[Discord] : "+mess+"</font>";
				console.log('BOT: '+mess);
				mp.players.forEach(_player => { _player.outputChatBox(str); });
			}
		},

		"goint": (player:PlayerMp, args:any[]) => {
			if (player.info.auth) {
				if (player.info.access >= 3) {
					var interiorid = parseInt(args[1]);
					if (interiorid != null) {
						player.position = new mp.Vector3(Objects.Interiors[interiorid].x,Objects.Interiors[interiorid].y,Objects.Interiors[interiorid].z);
						player.outputChatBox("Вы были перемещены в "+Objects.Interiors[interiorid].name);
					} else player.outputChatBox("Нужно вводить  /goint [interion_id]");
				} else player.outputChatBox("Ошибка прав доступа.");
			} else Game.utility.proximityMessage(25.0, player, player.info.nick + "<b>(" + player.id + ")</b>: *Голос животного");
		},

		"repair" : (player:PlayerMp, args:any[]) => {
			if (player.info.auth) player.vehicle.repair();
			else Game.utility.proximityMessage(25.0, player, player.info.nick + "<b>(" + player.id + ")</b>: *Голос животного");
		},
		
		"slapmepls": (player:PlayerMp, args:any[]) => {
			if (player.info.auth) {
				var pos = player.position;
				pos.z += 2.0;
				
				player.position = pos;
			} else Game.utility.proximityMessage(25.0, player, player.info.nick + "<b>(" + player.id + ")</b>: *Голос животного");
		},
		
		"veh": (player:PlayerMp, args:any[]) => {
			if (player.info.auth) {
				if (args[1] == "cars" || args[1] == "planes" || args[1] == "bikes" || args[1] == "trucks" || args[1] == "boats") {
					var pos = player.position;
					pos.x += 2.0;
					
					let temp = mp.vehicles.new(Objects.Vehicles[args[1]][args[2]], pos);
	
					temp.dimension = player.dimension;
					temp.info = {
						id: parseInt(temp.id),
						petrol: 100,
						respawned: false
					};
	
					/*temp.info = {
						owner: player.name,
						respawned: false,
						fraction: 0,
					};*/
					player.outputChatBox('Авто создано его ID: '+temp.id);
					player.outputChatBox('[DEBUG] Авто создано его ID: '+temp.id);
				} else {
					player.outputChatBox('Ошибка команды: тип транспорта один из cars,planes,bikes,trucks,boats');
					player.outputChatBox('[DEBUG] Не верный тип транспорта');
				}
			} else Game.utility.proximityMessage(25.0, player, player.info.nick + "<b>(" + player.id + ")</b>: *Голос животного");
		},

		"delveh": (player:PlayerMp, args:any[]) => {
			if (player.info.auth) {
				if (player.vehicle) {
					let veh = player.vehicle;
					let vehid = veh.id;
					setTimeout(() => { 
						player.removeFromVehicle()  
						veh.destroy();
						player.outputChatBox('Авто было удалено');
						player.outputChatBox('[DEBUG] Авто было удалено, его ID: '+vehid);
						let str = new Buffer(JSON.stringify({incar: false})).toString("hex");
				        player.outputChatBox("<script>server_package_('" + str + "');</script>");
				        player.outputChatBox("<script>INTERFACE.ui.updateSpeedo();INTERFACE.ui.showSpeedo(false);</script>");
					}, 500);
				} else {
					player.outputChatBox('Вы не в транспорте');
				}
			} else Game.utility.proximityMessage(25.0, player, player.info.nick + "<b>(" + player.id + ")</b>: *Голос животного");
		},

		"setweather": (player:PlayerMp, args:any[]) => {
			if (player.info.auth) mp.environment.weather = args[1];
			else Game.utility.proximityMessage(25.0, player, player.info.nick + "<b>(" + player.id + ")</b>: *Голос животного");
		},

		"settime": (player:PlayerMp, args:any[]) => {
			if (player.info.auth) mp.environment.time.hour = parseInt(args[1]);
			else Game.utility.proximityMessage(25.0, player, player.info.nick + "<b>(" + player.id + ")</b>: *Голос животного");
		},

		"goto": (player:PlayerMp, args:any[]) => {

			if (player.info.auth) {
				if (!args[1].length) {
					// Если что-то в команде было введено не так, то выводим соответствующее сообщение
					player.outputChatBox('Для отправки личного сообщения введите:<br /><b>/goto [player_id]</b>');
					return false;
				}

				const target = Other.findPlayerByIdOrNickname(args[1]);
				if (!target) {
					player.outputChatBox("Игрока нет или он не в игре.");
					return false;
				}
				var pos = target.position;
				pos.y +=2;
				player.position = pos;
				player.outputChatBox("Вы переместились к игроку"+target.info.nick);
			} else Game.utility.proximityMessage(25.0, player, player.info.nick + "<b>(" + player.id + ")</b>: *Голос животного");
		},

		"tp": (player:PlayerMp, args:any[]) => {

			if (player.info.auth) {
				if (!args[1].length || !args[2].length) {
					// Если что-то в команде было введено не так, то выводим соответствующее сообщение
					player.outputChatBox('Для перемещения игрока к игроку введите:<br /><b>/tp [player_id] [player_id]</b>');
					return false;
				}
				const target1 = Other.findPlayerByIdOrNickname(args[1]);
				const target2 = Other.findPlayerByIdOrNickname(args[1]);
				if (!target1) {
					player.outputChatBox("Игрокa#1 нет или он не в игре.");
					return false;
				}
				if (!target2) {
					player.outputChatBox("Игрокa#2 нет или он не в игре.");
					return false;
				}

				var pos = target2.position;
				pos.y +=2;
				target1.position = pos;

				player.outputChatBox("Вы переместили игрока"+target1.info.nick+" к игроку "+target2.info.nick+"");
				target1.outputChatBox("Вас переместили к игроку "+target2.info.nick+"");
			} else Game.utility.proximityMessage(25.0, player, player.info.nick+"<b>("+player.id+")</b>: *Голос животного");
		},

		"setskin": (player:PlayerMp, args:any[]) => {
			if (player.info.auth) {
				var skincount:number = Objects.Models.length-1;
				let skinid:number = parseInt(args[1]);
				if (skinid >= 0 && skinid<=skincount) {
					player.model = Objects.Models[args[1]];
					player.info.weapons.forEach( weapon => { player.giveWeapon(mp.joaat(weapon.model), weapon.ammo); });
					player.outputChatBox("Скин изменен на: "+Objects.Models[args[1]]);
				} else player.outputChatBox("Нужно вводить  /setskin [skin_id] от 0 до "+skincount);
			} else Game.utility.proximityMessage(25.0, player, player.info.nick+"<b>("+player.id+")</b>: *Голос животного");
		},

		"giveskin": (player:PlayerMp, args:any[]) => {
			if (player.info.auth) {
				var skincount:number = Objects.Models.length-1;
				let target = parseInt(args[1]);
				if (!target) {
					player.outputChatBox("Нужно вводить  /giveskin [player_id] [skin_id] от 0 до "+skincount);
					return false;
				}
				const recipient:PlayerMp = Other.findPlayerByIdOrNickname(target);
				if (!recipient) {
					player.outputChatBox("Игрокa нет или он не в игре.");
					return false;
				}

				let skinid = parseInt(args[2]);
				if (skinid >= 0 && skinid<=skincount) {
					recipient.model = Objects.Models[skinid];
					player.info.weapons.forEach( weapon => { player.giveWeapon(mp.joaat(weapon.model), weapon.ammo); });
					player.outputChatBox("Скин игрока "+recipient.info.nick+" изменен на: "+Objects.Models[skinid]);
				} else player.outputChatBox("Нужно вводить  /giveskin [player_id] [skin_id] от 0 до "+skincount);
			} else Game.utility.proximityMessage(25.0, player, player.info.nick + "<b>(" + player.id + ")</b>: *Голос животного");
		},

		"clothes": (player:PlayerMp, args:any[]) => {
			if (player.info.auth) player.setClothes(parseInt(args[1]), parseInt(args[2]), parseInt(args[3]), parseInt(args[4]));
			else Game.utility.proximityMessage(25.0, player, player.info.nick + "<b>(" + player.id + ")</b>: *Голос животного");
		},
		
		"prop": (player:PlayerMp, args:any[]) => {
			if (player.info.auth) player.setProp(parseInt(args[1]), parseInt(args[2]), parseInt(args[3]));
			else Game.utility.proximityMessage(25.0, player, player.info.nick + "<b>(" + player.id + ")</b>: *Голос животного");
		},

		"getgun": (player:PlayerMp, args:any[]) => {
			if (player.info.auth) {
				let type = parseInt(args[1]);
				let id = parseInt(args[2]);
				let ammo = parseInt(args[3]);
				if (type && id && ammo) {
					player.giveWeapon(mp.joaat(Objects.Weapons[type][id]),ammo);
					player.outputChatBox("Выдано оружие: "+Objects.Weapons[type][id]);
				} else player.outputChatBox(`Нужно вводить  /weapon [type_id] [weapon_id] [ammo]`);
			} else Game.utility.proximityMessage(25.0, player,player.info.nick+"<b>("+player.id+")</b>: *Голос животного");
		},
		"givegun": (player:PlayerMp, args:any[]) => {
			if (player.info.auth) {
				let target = parseInt(args[1]);
				if (target) {
					const recipient = Other.findPlayerByIdOrNickname(target);
					if (!recipient) {
						player.outputChatBox("Игрокa нет или он не в игре.");
						return false;
					}
					let type = parseInt(args[2]);
					let weapid = parseInt(args[3]);
					let ammo = parseInt(args[4]);
					if (type && weapid && ammo) {
						recipient.giveWeapon(mp.joaat(Objects.Weapons[type][weapid]),ammo);
						player.outputChatBox("Выдано оружие: "+Objects.Weapons[type][weapid]);
						recipient.outputChatBox("Выдано оружие: "+Objects.Weapons[type][weapid]);
					} else player.outputChatBox("Нужно вводить  /givegun [player_id] [type_id] [weapon_id] [ammo]");
				} else player.outputChatBox("Нужно вводить  /givegun [player_id] [type_id] [weapon_id] [ammo]");
			} else Game.utility.proximityMessage(25.0, player, player.info.nick+"<b>("+player.id+")</b>: *Голос животного");
		}
	},
	support: {
		"pos": (player, args) => {
			if (player.info.auth) {
				var pos = player.position;
				player.outputChatBox("[DEBUG] X: <b>" + pos.x + "</b>, Y: <b>" + pos.y + "</b>, Z: <b>" + pos.z + "</b>");
			} else Game.utility.proximityMessage(25.0, player, player.info.nick + "<b>(" + player.id + ")</b>: *Голос животного");
		},
		"debuglogs": (player, args) => {
			if (player.info.auth) {
				if (player.info.access >= 6) {
					player.outputChatBox("<script>INTERFACE.ui.showDebug();</script>");
					player.outputChatBox("Консоль разработчика была включена/выключена");
					player.outputChatBox("[DEBUG] Консоль разработчика была включена/выключена");
				} else {
					player.outputChatBox("У вас нет доступа к этой команде!");
				}
			} else Game.utility.proximityMessage(25.0, player, player.info.nick + "<b>(" + player.id + ")</b>: *Голос животного");
		}
	},
	player: {
		"pm": (player, args) => {
		    // Check args
		    if (player.info.auth) {
			    if (args.length < 3 || !args[1].length || !args[2].length) {
			      	player.outputChatBox('Для отправки личного сообщения введите:<br /><b>/pm [player_id] [message]</b>');
	
			      	return false;
			    }
	
			    // Search recipient by second argument
			    const recipient = Other.findPlayerByIdOrNickname(args[1]);
	
			    // If recipient not found show special message
			    if (!recipient) {
			      	player.outputChatBox("Игрокa нет или он не в игре.");
	
			      	return false;
			    }
	
			    // Source message
			    const message = args.slice(2).join(' ');
			    // Generate chat string in the following format:
			    // [PM] Sender_Nickname[Sender_Id] -> Recipient_Nickname[Recipient_Id]: Text message
			    recipient.outputChatBox("SMS от "+player.info.nick+"["+player.id+"] : "+message+".");
			    player.outputChatBox("SMS отправлено "+recipient.info.nick+"["+recipient.id+"] : "+message+".");
			    // Send message to recipient
			    //recipient.outputChatBox(`<b>SMS получено ${player.name}[${player.id}]</b> : ${message}`);
			    // Send message to sender
			    //player.outputChatBox(`<b>SMS отправлено ${recipient.name}[${recipient.id}]</b> : ${message}`);
			} else Game.utility.proximityMessage(25.0, player, player.info.nick + "<b>(" + player.id + ")</b>: *Голос животного");
		},
	    "register": (player, args) => {
			if (!player.info.auth) { 
				var login = args[1];
				var password = args[2];
				var nick = args[3];
				var email = args[4];
				var isPanel = args[5];
				if (login && password && nick && email) {
					let EMAIL_REGEX = "^[a-zA-Z0-9_\./-]+@[a-zA-Z0-9/-]+\.[a-zA-Z\.]+$";
					let NICK_REGEX = "^[a-zA-Z]+_[a-zA-Z]+$";
					if (args[1].length <= 3 || args[1].length > 16) { player.outputChatBox("[Регистрация] Длина логина от 4 до 16 символов"); return false; }
					if (args[2].length < 6 || args[2].length > 18) { player.outputChatBox("[Регистрация] Длина пароля от 6 до 18 символов"); return false; }
					if (email.search(EMAIL_REGEX)) { player.outputChatBox("[Регистрация] Введите правильный формат E-mail (Например: email@example.ru)"); return false; }
					if (nick.search(NICK_REGEX)) { player.outputChatBox("[Регистрация] Введите правильный формат ника (Например: Ivan_Ivanov)"); return false; }
					let weapons = [{name: "weapon_pistol", ammo: 1000}, {name: "weapon_SMG", ammo: 1000}, {name: "weapon_SniperRifle", ammo: 1000}, {name: "weapon_RPG", ammo: 1000}, {name: "weapon_Firework", ammo: 1000}];
					var newplayer:database.IDB_PlayerData = {
						login: login, 
						password: password,
					    email: email,
					    nick: nick,
					    skin: Objects.Models[291],
					    health: 100,
					    armor: 0,
					    eat: 100,
					    money: 500,
					    bank: 0,
					    weapons: JSON.stringify(weapons),
					    arrests: 0,
					    stars: 0,
					    fraction: 0,
					    rank: 0,
					    drugs: 0,
					    posx: -415.777,
						posy: 1168.791,
						posz: 325.854,
						ban: 0,
						access: 0
					};
					var geterr = 0;
					database.getPlayerDataBylogin(login, function(err, playerdata:database.IDB_PlayerData) {
						if (err) {
							geterr = 1;
							if (isPanel == "panel") { player.outputChatBox("<script>$('.error-message-register').html('Ошибка регистрации, повторите попытку');</script>"); return false; } 
							else { player.outputChatBox("[Регистрация] Ошибка регистрации, повторите попытку."); return false; }
						}
						if (playerdata == undefined) {
							console.log("Empty Login ");
							database.addPlayerData(newplayer, function(err) {
								console.log("Add player callback ");
								if (err) {
									if (isPanel == "panel") { player.outputChatBox("<script>$('.error-message-register').html('Ошибка регистрации, повторите попытку');</script>"); return false; } 
									else { player.outputChatBox("[Регистрация] Ошибка регистрации, повторите попытку."); return false; }
								}
								console.log("End additing ");
								if (isPanel == "panel") { 
									player.outputChatBox("<script>$('.error-message-auth').html('Регистрация успешна!');</script>");
									player.outputChatBox("<script>$('.error-message-register').html(' ');INTERFACE.ui.showRegister(false);INTERFACE.ui.showAuth(true);</script>");
								} 
								else { player.outputChatBox("[Регистрация] Регистрация успешна!<br />Авторизируйтесь введя команду /auth [login] [password]"); }
							});
						} else { 
							geterr = 2;
							if (isPanel == "panel") { player.outputChatBox("<script>$('.error-message-register').html('Такой логин уже зарегистрирован');</script>"); return false; } 
							else { player.outputChatBox("[Регистрация] Такой логин уже зарегистрирован"); return false; }
						}
					});
				} else player.outputChatBox("Нужно вводить  /register [login] [password] [nick_name] [email]");
			}
		},
		"auth": (player, args) => { 
			if (!player.info.auth) {
				var login = args[1];
				var password = args[2];
				var isPanel = args[3];
				if (login && password) {
					database.getPlayerDataBylogin(login, function(err, playerdata:database.IDB_PlayerData) {
						if (err) {
							if (isPanel == "panel") { player.outputChatBox("<script>$('.error-message-register').html('Ошибка регистрации, повторите попытку');</script>"); return false; } 
							else { player.outputChatBox("[Регистрация] Ошибка регистрации, повторите попытку."); return false; }
						}
						if (playerdata.login != "") {
							if (playerdata.ban == "1") {
								if (isPanel == "panel") { player.outputChatBox("<script>$('.error-message-auth').html('Данный аккаунт забанен!');</script>"); return false; } 
								else { player.outputChatBox("[Авторизация] Данный аккаунт забанен"); return false; }
							}
							else {
								if (playerdata.password != password) {
									if (isPanel == "panel") { player.outputChatBox("<script>$('.error-message-auth').html('Не верный пароль!');</script>"); return false; } 
									else { player.outputChatBox("[Авторизация] Не верный пароль"); return false; }
								} else {
									player.info = {
										auth: true,
									    bd_id: parseInt(playerdata.id),
										nick: playerdata.nick,
									    eat: parseInt(playerdata.eat),
									    money: parseInt(playerdata.money),
									    bank: parseInt(playerdata.bank),
									    weapons: JSON.parse(playerdata.weapons),
									    arrests: parseInt(playerdata.arrests),
									    stars: parseInt(playerdata.stars),
									    fraction: parseInt(playerdata.fraction),
									    rank: parseInt(playerdata.rank),
									    drugs: parseInt(playerdata.drugs),
									    access: parseInt(playerdata.access),
										ban: parseInt(playerdata.ban)
									}
									player.name = playerdata.nick+'('+player.id+')';
									player.model = parseInt(playerdata.skin);
									let pos:Vector3 = new mp.Vector3(parseFloat(playerdata.posx),parseFloat(playerdata.posy),parseFloat(playerdata.posz));
									player.spawn(pos);

									console.log("DB_ID:"+parseInt(playerdata.id)+"|ID:"+player.id+"|Gr:"+player.info.access+"|Name:"+playerdata.nick);
									player.info.weapons.forEach( weapon => { player.giveWeapon(mp.joaat(weapon.name), weapon.ammo); })
									if (isPanel == "panel") { 
										console.log('close panel');
										player.outputChatBox("<script>$('.error-message-auth').html(' ');</script>");
										player.outputChatBox("<script>INTERFACE.ui.showAuth(false);</script>");
									}
									player.outputChatBox("[Авторизация] Вы успешно авторизировались");
									player.outputChatBox("Для просмотра всех возможных комманд нажмите F3");


									var clientJSON = player.info;
									clientJSON.id = player.id;
									clientJSON.position = player.position;
									let str = new Buffer(JSON.stringify(clientJSON)).toString("hex");
									player.outputChatBox("<script>server_package('" + str + "', 'player');</script>");
									player.outputChatBox("<script>INTERFACE.ui.updateInterface();</script>");
								}
							}
						} else {
							if (isPanel == "panel") { player.outputChatBox("<script>$('.error-message-auth').html('Такой логин не зарегистрирован');</script>"); } 
							else { player.outputChatBox("[Авторизация] Такой логин не зарегистрирован"); return false; }
						}
					});
				} else player.outputChatBox("[Авторизация] Нужно вводить  /auth [login] [password]");
			}
		}
	}
}

//export = new servercommands();