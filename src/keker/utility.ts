export namespace Utility {
    export class Sphere {
        constructor(public x: number,
                    public y: number,
                    public z: number,
                    public radius: number) {}

        public inRangeOfPoint(position: any): boolean {
            return (Math.pow((position.x - this.x), 2) +
            Math.pow((position.y - this.y), 2) +
            Math.pow((position.z - this.z), 2) < Math.pow(this.radius, 2));
        }
    }

    export function PlayerToPoint(range: number, player: any, x: number, y: number, z: number) {
        return new Sphere(x, y, z, range).inRangeOfPoint(player.position);
    }

    export function proximityMessage(radi: number, sender: any, message: string) {
        mp.players.forEach(_player => {
            if (Utility.PlayerToPoint(radi, _player, sender.position.x, sender.position.y, sender.position.z)/* && receiver.dimension == sender.dimension*/) {
                _player.outputChatBox(message);
            }
        });
    }
}