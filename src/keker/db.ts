var mysql = require('mysql'); 

//Берём конфиг из файла, т.к. данные могут отличатсья для разных машин и т.п.
var db_config = require("../../db_config.json");

var connection;

//Коннект и автоматический реконнект при потере соединения
function Reconnect() {
    connection = mysql.createConnection(db_config); 

    connection.connect(function(err) {              
        if(err) {                                     
            console.log('Error when connecting to db:', err);
            setTimeout(Reconnect(), 2000); 
        }
        else
            console.log("Database connected");                                     
    });                                     
                                          
    connection.on('error', function(err) {
        console.log('Db error', err);
        if(err.code === 'PROTOCOL_CONNECTION_LOST') { 
           console.log('Database connection lost: reconnecting....');
            Reconnect();                         
        } else {                                      
            throw err;                                  
        }
    });
}


//Коннектимся к базе
Reconnect();

export interface IDB_PlayerData {
    login?: string;
    password?: string;
    email?: string;
    nick?: string;
    skin: any;
    health: any;
    armor: any;
    eat: any;
    money: any;
    bank: any;
    weapons: string;
    arrests: any;
    stars: any;
    fraction: any;
    rank: any;
    drugs: any;
    posx: any;
	posy: any;
	posz: any;
    ban: any;
    id?: any;
    access?: any;
}

export interface IDB_VehicleData {
    id: any;
    model: any;
    posx: any;
	posy: any;
	posz: any;
    resp_posx: any;
	resp_posy: any;
	resp_posz: any;
    health: any;
    fraction: any;
    petrol: any;
    owner: string;
    respawned: boolean;
    bagage: string;
}


//Добавление игрока в базу
export function addPlayerData(playerData: IDB_PlayerData, callback: (err) => any){
    
    console.log("Adding Player To Database...");
    connection.query("INSERT INTO accounts SET ?" , [playerData], function(err, res) {
        
        if (!err)
            console.log("Adding Player To Database: OK");
        else
            console.log(err);
        
        if (callback && callback != null)
            callback(err)            
        
    });
};    

//Загрузка данных игрока из базы
export function getPlayerDataBylogin(playerlogin: string, callback: (err, playerData: IDB_PlayerData) => any){
    
    console.log('Getting player data...'); 
        
    connection.query("SELECT * FROM accounts WHERE login = ?", [playerlogin], function(err, res) {
                        
        if (!err){
            console.log('Getting Player data OK');
            //console.log('Player data: ' + JSON.stringify(res[0]));
        }
        else
            console.log(err);
               
        if (callback && callback != null) {
            callback(err, res[0]);     
        } 
    });
};

//Сохранение данных игрока в базу
export function setPlayerDataByID(id: number, playerData: IDB_PlayerData, callback: (err) => any){
            
    console.log("Saving Player To Database...");
    
    connection.query("UPDATE accounts SET ? WHERE id = ?", [playerData, id], function(err) {
        
        if (!err){
            console.log("Saving Player To Database: OK");
         }else
            console.log(err);


        if (callback && callback != null)
            callback(err);            
            
    });
};

