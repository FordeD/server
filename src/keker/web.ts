import * as fs from "fs";
import * as path from "path";
import * as http from "http";
import * as url from "url";

namespace Web {
	const mimeType = { // mime типы по расширениям файлов
		'.ico': 'image/x-icon',
		'.html': 'text/html',
		'.js': 'text/javascript',
		'.json': 'application/json',
		'.css': 'text/css',
		'.png': 'image/png',
		'.jpg': 'image/jpeg',
		'.wav': 'audio/wav',
		'.mp3': 'audio/mpeg',
		'.svg': 'image/svg+xml',
		'.pdf': 'application/pdf',
		'.doc': 'application/msword',
		'.eot': 'appliaction/vnd.ms-fontobject',
		'.ttf': 'aplication/font-sfnt'
	};
	http.createServer(function (req, res) {
		res.setHeader('Access-Control-Allow-Origin', '*'); // разрешаем кросс-деменые запросы
		let parsedUrl = url.parse(req.url); // отсекаем от url все лишнее
	    let filePath = __dirname+'/ui' + parsedUrl.pathname; // Парсим url в путь к файлу
	    let ext = path.extname(filePath); // получаем расширение файла
		
	    if(req.url=="/api/players_list.json"){ // отдельная ссылка для генерации JSON списка игроков
			let pl = {
				online: mp.players.length,
				slots: mp.players.size,
				players: []
			}
			mp.players.forEach(player => {
				pl.players.push({ name: player.name, ip: player.ip, ping: player.ping, id: player.id });
			});
			res.writeHead(200, { 'Content-Type': mimeType['.json'] });
			res.end(JSON.stringify(pl), 'utf-8');
		} else {
			fs.readFile(filePath, function(error, content) {
				if (error) {
					if(error.code == 'ENOENT'){ // если файл отсутсвует
						res.writeHead(404, { 'Content-Type': 'text/plain' });
						res.end('404 Not Found');
					}
					else { // другие ошибки
						res.writeHead(500);
						res.end('Error: '+error.code+' ..\n');
					}
				}
				else {
					res.writeHead(200, { 'Content-Type': mimeType[ext] || 'text/plain' });
					res.end(content, 'utf-8');
				}
			});
		}
	}).listen(4499); // вешаем наш веб сервер на свободный порт, у меня это 8080
}

export = Web;